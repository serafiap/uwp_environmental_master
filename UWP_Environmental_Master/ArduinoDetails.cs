﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UWPSerialCommunication;
using UWPSerialCommunication.Arduino;
using Windows.System.Threading;
using UWP_Environmental_Master.ArduinoControllables;

namespace UWP_Environmental_Master
{
    public static class ArduinoDetails
    {
        public static SerialCommunicator Communicator = new SerialCommunicator();
        private static ThreadPoolTimer loopThread;

        //List of peripherals that will periodically have their states read
        public static List<IArduinoReadable> ReadingList;

        //Defining sensors
        #region 
        public static DHT11Sensor DHT11Sensor1 = new DHT11Sensor((int)ArduinoPins.DHT11_1);
        public static DHT11Sensor DHT11Sensor2 = new DHT11Sensor((int)ArduinoPins.DHT11_2);
        #endregion

        //DefiningControllables
        #region
        public static TemperatureRelayAutomaticControllable Heater1 = new TemperatureRelayAutomaticControllable(
            (int)ArduinoPins.HeatRelay1, PinState.Low, PinState.High, 75, 90);
        public static DCFanControllable Humidifier1Fan1 = new DCFanControllable((int)ArduinoPins.Fan1, PinState.High, PinState.Low);
        public static UltrasonicHumidityAutomaticControllable Humidifier1Mister1 = new UltrasonicHumidityAutomaticControllable(
            (int)ArduinoPins.MisterRelay1, PinState.Low, PinState.High, 50, 60, Humidifier1Fan1);
        #endregion


        public static void Start()
        {
            ReadingList = new List<IArduinoReadable>()
            {
                DHT11Sensor1,
                DHT11Sensor2,
                Heater1,
                Humidifier1Fan1,
                Humidifier1Mister1
            };

            BeginCommunication();
        }

        private static void BeginCommunication()
        {
            var setupThread = ThreadPoolTimer.CreatePeriodicTimer(SetupTimer_Tick, TimeSpan.FromMilliseconds(0));
        }

        private static async void SetupTimer_Tick(ThreadPoolTimer threadPoolTimer)
        {
            try
            {
                await Communicator.ConnectAsync();
                await Communicator.WriteAsync(Crypter.SetPinMode((int)ArduinoPins.Fan1, PinMode.Output));
                await Communicator.WriteAsync(Crypter.SetPinMode((int)ArduinoPins.HeatRelay1, PinMode.Output));
                await Communicator.WriteAsync(Crypter.SetPinMode((int)ArduinoPins.MisterRelay1, PinMode.Output));
                foreach (var item in ReadingList)
                {
                    if (item is IArduinoControllable)
                    {
                        await((IArduinoControllable)item).Setup();
                    }
                }
                loopThread = ThreadPoolTimer.CreatePeriodicTimer(LoopTimer_Tick, TimeSpan.FromSeconds(5));
            }
            catch (Exception)
            {
            }
        }


        private static async void LoopTimer_Tick(ThreadPoolTimer threadPoolTimer)
        {
            try
            {
                await RequestReadings();
                await TriggerAutomation();
            }
            catch (Exception)
            {
                //Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                //{
                //    //textTB.Text = arduinoCommunicator.Listening.ToString();
                //});
            }

            if (Communicator.Listening == false)
            {
                loopThread.Cancel();
                BeginCommunication();
            }
        }

        private static async Task RequestReadings()
        {
            foreach (var readable in ReadingList)
            {

                if (readable.GetType() == typeof(DHT11Sensor))
                {
                    await Communicator.WriteAsync(Crypter.ReadDHT11(readable.Pin));
                }


                else if (readable is IArduinoControllable)
                    await Communicator.WriteAsync(Crypter.DigitalRead(readable.Pin));
            }
        }

        private static async Task TriggerAutomation()
        {
            foreach (var item in ReadingList)
            {
                if (item is IArduinoAutomaticControllable)
                {
                    await ((IArduinoAutomaticControllable)item).ActOnLimits();
                }
            }
        }

        //TODO: Manage incoming analog results
        public static void ProcessAndStoreIncomingResults(ArduinoReading reading)
        {
            int i = ReadingList.IndexOf(ReadingList.Where(p => p.Pin == reading.Pin).FirstOrDefault());

            try
            {
                switch (reading.ReadingType)
                {
                    case CommandType.ReadAll:
                        break;
                    case CommandType.SetPinMode:
                        break;
                    case CommandType.DigitalRead:
                        if (ReadingList[i] is IArduinoControllable)
                        {
                            IArduinoControllable digitalReading = (IArduinoControllable)ReadingList[i];
                            //TODO line below may only record to the newly created object
                            digitalReading.RecordPinState((PinState)reading.Reading1);
                        }
                        break;
                    case CommandType.DigitalWrite:
                        break;
                    case CommandType.AnalogRead:
                        break;
                    case CommandType.AnalogWrite:
                        break;
                    case CommandType.ReadDHT11:
                        IArduinoSensor sensorReading = (IArduinoSensor)ReadingList[i];
                        //TODO line below may only record to the newly created object
                        sensorReading.Reading1 = reading.Reading1;
                        sensorReading.Reading2 = reading.Reading2;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                //Ya dun mest up
            }
        }

        public static string ExtractSensorData(ArduinoPins sensor, int readingNumber)
        {
            switch (readingNumber)
            {
                case 1:
                    return ((IArduinoSensor)ReadingList.Where(i => i.Pin == (int)sensor).ToList()[0]).Reading1.ToString();
                case 2:
                    return ((IArduinoSensor)ReadingList.Where(i => i.Pin == (int)sensor).ToList()[0]).Reading2.ToString();
                default:
                    return "Invalid readingNumber";
            }

        }
    }



    public enum ArduinoPins
    {
        DHT11_1 = 2,
        DHT11_2 = 3,
        HeatRelay1 = 6,
        Fan1 = 13,
        MisterRelay1 = 11

    }
}
