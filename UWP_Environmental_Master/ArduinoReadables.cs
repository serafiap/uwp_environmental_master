﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWP_Environmental_Master
{
    public interface IArduinoReadable
    {
        int Pin { get; }
    }

    public interface IArduinoSensor : IArduinoReadable
    {
        double Reading1 { get; set; }
        double Reading2 { get; set; }
    }
    public class DHT11Sensor : IArduinoSensor
    {
        public int Pin { get; }
        public double Reading1 { get; set; }
        public double Reading2 { get; set; }

        public double Temperature
        {
            get { return Reading2; }
            set { Reading2 = value; }
        }

        public double Humidity
        {
            get { return Reading1; }
            set { Reading1 = value; }
        }

        public DHT11Sensor(int pin)
        {
            Pin = pin;
        }
    }
}
