﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using UWPSerialCommunication;
using UWPSerialCommunication.Arduino;
using UWP_Environmental_Master.ArduinoControllables;
using System.Threading;
using System.Threading.Tasks;
using Windows.System.Threading;
using Windows.UI.Core;
using UWP_Environmental_Master.API;
using System.Net;

namespace UWP_Environmental_Master
{
    using AD = ArduinoDetails;
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            DispatcherTimer systemTime = new DispatcherTimer();
            systemTime.Interval = new TimeSpan(0, 0, 0, 1);
            systemTime.Tick += SystemTime_Tick;
            SystemTime_Tick(this, new object());
            systemTime.Start();

            AD.Communicator.StringReceived += Communicator_StringReceived;
            AD.Start();
        }

        private void SystemTime_Tick(object sender, object e)
        {
            //Quick test to see if there is an active connection to the internet
            connectionTB.Text = "Tessting Connection";
            try
            {
                using (var client = new WebClient())
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        connectionTB.Text = "Internet Connected";
                    }
                }
            }
            catch (Exception ee)
            {
                connectionTB.Text = "Internet Not Connected";
            }

            string s = DateTime.Now.DayOfWeek.ToString() + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm");
            timeTB.Text = s;
        }

        private async void Communicator_StringReceived(object sender, EventArgs e)
        {
            if (AD.Communicator.StringsAvailable)
            {
                if (AD.Communicator.IncomingStringsQueue.Count > 0)
                {
                    ArduinoReading oldestReading = new ArduinoReading(AD.Communicator.IncomingStringsQueue.Dequeue());
                    AD.ProcessAndStoreIncomingResults(oldestReading);
                }
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    FillTextBlocks();
                });
            }
        }

        private void FillTextBlocks()
        {
            //Region 1
            temperature1TB.Text = AD.ExtractSensorData(ArduinoPins.DHT11_1, 2);
            humidity1TB.Text = AD.ExtractSensorData(ArduinoPins.DHT11_1, 1);

            //Region 2
            temperature2TB.Text = AD.ExtractSensorData(ArduinoPins.DHT11_2, 2);
            humidity2TB.Text = AD.ExtractSensorData(ArduinoPins.DHT11_2, 1);

            //Region 3
            IArduinoControllable derp = (IArduinoControllable)AD.ReadingList[3];
            heatStateTB.Text = IntendedStates.HeatState;
            fanStateTb.Text = IntendedStates.FanState;
            misterStateTB.Text = IntendedStates.MisterState;

            //Region 4
            temperature4TB.Text = this.ActualWidth.ToString();
            humidity4TB.Text = this.ActualHeight.ToString();

        }
    }
}
