﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Web.Http;

namespace UWP_Environmental_Master.API
{
    public static class Get
    {

        public static async Task<string> FromBodyAsync(string url)
        {
            HttpClient hClient = new HttpClient();
            Uri requestUri = new Uri(url);
            HttpResponseMessage hResponse = new HttpResponseMessage();
            string hResponseBody = "x";

            try
            {
                hResponse = await hClient.GetAsync(requestUri);
                hResponse.EnsureSuccessStatusCode();
                hResponseBody = await hResponse.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {

                throw;
            }
            return hResponseBody.Substring(1,hResponseBody.Length-2);
        }
    }
}
