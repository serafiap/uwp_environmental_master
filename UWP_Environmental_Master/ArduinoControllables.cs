﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UWPSerialCommunication.Arduino;
using UWPSerialCommunication;
using UWP_Environmental_Master;
using Windows.System.Threading;

namespace UWP_Environmental_Master.ArduinoControllables
{
    using HF = HelpfullFunctions;
    //TODO: Create a controllable for MistKing sprayer

    using AD = ArduinoDetails;
    //TODO: It turns out, this is only good for digital controllables with a single pin
    interface IArduinoControllable : IArduinoReadable
    {
        PinState OnState { get; }
        PinState OffState { get; }
        PinState CurrentState { get; }

        Task Setup();
        Task TurnOn();
        Task TurnOff();
        void RecordPinState(PinState currentState);


    }

    /// <summary>
    /// Interface for controlables that are automatically turned on and off based on external factors
    /// </summary>
    interface IArduinoAutomaticControllable : IArduinoControllable
    {
        int LowerLimit { get; }
        int UpperLimit { get; }

        Task ActOnLimits();
        Task ActOnLimits(int currentState);
    }

    //Controllables
    #region 
    public class DCFanControllable : IArduinoControllable
    {
        public int Pin { get; set; }
        public PinState OnState { get; set; }
        public PinState OffState { get; set; }
        public PinState CurrentState { get { return _CurrentState; } }
        private PinState _CurrentState;

        public DCFanControllable(int pin, PinState on, PinState off)
        {
            Pin = pin;
            OnState = on;
            OffState = off;
        }

        public async Task TurnOn()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OnState));
            IntendedStates.FanState = "On";
        }

        public async Task TurnOff()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OffState));
            IntendedStates.FanState = "Off";
        }

        public void RecordPinState(PinState currentState)
        {
            _CurrentState = currentState;
        }

        public async Task Setup()
        {

            await AD.Communicator.WriteAsync(Crypter.SetPinMode(Pin, PinMode.Output));
            await TurnOff();
        }
    }

    //TODO: Add controllable info for RGB lights
    //TODO: Add code elsewhere to add lights

    //For 12V strip, not WS2812

    public class RGBControllable : IArduinoControllable
    {
        public int Pin { get { return rPin; } } //Deep breaths.
        public int rPin { get { return _rPin; } } //Pin for red light
        public int gPin { get { return _gPin; } } //Pin for green light
        public int bPin { get { return _bPin; } } //Pin for blue light

        private int _rPin;
        private int _gPin;
        private int _bPin;

        private int _redTarget;
        private int _greenTarget;
        private int _blueTarget;

        public int rLevel = 0;
        public int gLevel = 0;
        public int bLevel = 0;

        private ThreadPoolTimer IncrementRed;
        private ThreadPoolTimer IncrementGreen;
        private ThreadPoolTimer incrementBlue;

        private ThreadPoolTimer SweepTimer;

        public PinState OnState { get; }
        public PinState OffState { get; }
        public PinState CurrentState { get { return _CurrentState; } }
        private PinState _CurrentState;


        public RGBControllable(int redPin, int greenPin, int bluePin, PinState on, PinState off)
        {
            _rPin = redPin;
            _gPin = greenPin;
            _bPin = bluePin;
            OnState = on;
            OffState = off;
        }

        public async Task Setup()
        {
            await AD.Communicator.WriteAsync(Crypter.SetPinMode(rPin, PinMode.Output));
            await AD.Communicator.WriteAsync(Crypter.SetPinMode(gPin, PinMode.Output));
            await AD.Communicator.WriteAsync(Crypter.SetPinMode(bPin, PinMode.Output));

            await TurnOff();
        }
        public async Task TurnOn()
        {
            await AD.Communicator.WriteAsync(Crypter.AnalogWrite(rPin, rLevel));
            await AD.Communicator.WriteAsync(Crypter.AnalogWrite(gPin, gLevel));
            await AD.Communicator.WriteAsync(Crypter.AnalogWrite(bPin, bLevel));
        }
        public async Task TurnOff()
        {
            await AD.Communicator.WriteAsync(Crypter.AnalogWrite(rPin, 0));
            await AD.Communicator.WriteAsync(Crypter.AnalogWrite(gPin, 0));
            await AD.Communicator.WriteAsync(Crypter.AnalogWrite(bPin, 0));
        }
        public void RecordPinState(PinState currentState)
        {

        }

        public void SetColors(int R, int G, int B)
        {
            rLevel = HF.Constrain(R, 0, 255);
            gLevel = HF.Constrain(G, 0, 255);
            bLevel = HF.Constrain(B, 0, 255);
        }

        //DOFIRST: Check over this
        public void Sweep(int RedEnd, int GreenEnd, int BlueEnd, TimeSpan time)
        {
            _redTarget = RedEnd;
            _greenTarget = GreenEnd;
            _blueTarget = BlueEnd;

            int secondsTime = (int)time.TotalSeconds;
            int rInterval = secondsTime / (Math.Abs(rLevel - RedEnd));
            int gInterval = secondsTime / (Math.Abs(gLevel - GreenEnd));
            int bInterval = secondsTime / (Math.Abs(bLevel - BlueEnd));



            try
            {
                incrementBlue.Cancel();
                IncrementGreen.Cancel();
                IncrementRed.Cancel();
                SweepTimer.Cancel();
            }
            catch (Exception)
            {
            }

            try
            {
                IncrementRed = ThreadPoolTimer.CreatePeriodicTimer(Red_Tick, new TimeSpan(0, 0, rInterval));
                IncrementGreen = ThreadPoolTimer.CreatePeriodicTimer(Green_Tick, new TimeSpan(0, 0, gInterval));
                incrementBlue = ThreadPoolTimer.CreatePeriodicTimer(Blue_Tick, new TimeSpan(0, 0, bInterval));

                SweepTimer = ThreadPoolTimer.CreatePeriodicTimer(Sweep_Tick, new TimeSpan(0, 0, 1)); //Only a fool would hardcode that.
            }
            catch (Exception)
            {
            }
        }

        private void Red_Tick(ThreadPoolTimer tpt)
        {
            if (rLevel != _redTarget)
            {
                rLevel += HF.ProgressToward(rLevel, _redTarget);
            }
            else IncrementRed.Cancel();
        }
        private void Green_Tick(ThreadPoolTimer tpt)
        {
            if (gLevel != _greenTarget)
            {
                gLevel += HF.ProgressToward(gLevel, _redTarget);
            }
            else IncrementGreen.Cancel();
        }
        private void Blue_Tick(ThreadPoolTimer tpt)
        {
            if (bLevel != _blueTarget)
            {
                bLevel += HF.ProgressToward(bLevel, _blueTarget);
            }
            else incrementBlue.Cancel();
        }

        private async void Sweep_Tick(ThreadPoolTimer tpt)
        {
            if (rLevel != _redTarget ||
                gLevel != _greenTarget ||
                bLevel != _blueTarget)
            {
                if (!ArduinoDetails.Communicator.Listening)
                {
                    await TurnOn();
                }
            }
        }
    }

    #endregion

    //AutomaticControllables
    #region
    /// <summary>
    /// Heating and cooling controllable based on temperature
    /// </summary>
    public class TemperatureRelayAutomaticControllable : IArduinoAutomaticControllable
    {
        public int Pin { get; }
        public PinState OnState { get; }
        public PinState OffState { get; }
        public int LowerLimit { get; }
        public int UpperLimit { get; }
        public PinState CurrentState { get { return _CurrentState; } }
        private PinState _CurrentState;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pin">Arduino pin</param>
        /// <param name="onState">Power level for turning the device on</param>
        /// <param name="offState">Power state for turning the device off</param>
        /// <param name="lowerLimit">Temperature(F) below which the device will disable</param>
        /// <param name="upperLimit">Temperature(F) above which the device will disable</param>
        public TemperatureRelayAutomaticControllable(int pin, PinState onState, PinState offState, int lowerLimit, int upperLimit)
        {
            Pin = pin;
            OnState = onState;
            OffState = offState;
            UpperLimit = upperLimit;
            LowerLimit = lowerLimit;
        }

        public async Task TurnOn()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OnState));
            IntendedStates.HeatState = "On";
        }

        public async Task TurnOff()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OffState));
            IntendedStates.HeatState = "Off";
        }

        public async Task ActOnLimits()
        {
            var sensorList = AD.ReadingList.Where(i => i.GetType() == typeof(DHT11Sensor)).ToList();//i.GetType() == typeof(DHT11Sensor));
            double avgTemp = 0;
            if (sensorList.Count() > 0)
            {
                foreach (var sensor in sensorList)
                {

                    avgTemp += ((DHT11Sensor)sensor).Temperature;
                }
                avgTemp = avgTemp / sensorList.Count();
            }
            await ActOnLimits((int)avgTemp);
        }

        public async Task ActOnLimits(int CurrentValue)
        {
            if (CurrentValue > UpperLimit)
            {
                await TurnOff();
            }

            else if (CurrentValue < LowerLimit)
            {
                await TurnOn();
            }
        }

        public void RecordPinState(PinState currentState)
        {
            _CurrentState = currentState;
        }

        public async Task Setup()
        {

            await AD.Communicator.WriteAsync(Crypter.SetPinMode(Pin, PinMode.Output));
            await TurnOff();
        }
    }

    /// <summary>
    /// Humidity controllable composed of an ultrasonic mister and distributed by controllable fans.  Based on humidity readings.
    /// </summary>
    public class UltrasonicHumidityAutomaticControllable : IArduinoAutomaticControllable
    {
        public int Pin { get; }
        public PinState OnState { get; }
        public PinState OffState { get; }
        public int LowerLimit { get; }
        public int UpperLimit { get; }
        public PinState CurrentState { get { return _CurrentState; } }
        private PinState _CurrentState;

        private TimeSpan _MaxRunTime = new TimeSpan(8, 0, 0);

        private List<DCFanControllable> _fan;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pin">Arduino pin</param>
        /// <param name="onState">Power level for turning the device on</param>
        /// <param name="offState">Power state for turning the device off</param>
        /// <param name="lowerLimit">Humidity (%RH) below which the device will disable</param>
        /// <param name="upperLimit">Humidity (%RH) above which the device will disable</param>
        /// <param name="fans">Collection of controllable fans for pushing air through humidifier</param>
        public UltrasonicHumidityAutomaticControllable(int pin, PinState onState, PinState offState, int lowerLimit, int upperLimit, params DCFanControllable[] fans)
        {
            Pin = pin;
            OnState = onState;
            OffState = offState;
            LowerLimit = lowerLimit;
            UpperLimit = upperLimit;
            _fan = fans.ToList();
        }

        //TODO: Add code for cycling the mister on and off using a new threadpooltimer
        public async Task TurnOn()
        {
            foreach (var fan in _fan)
            {
                await fan.TurnOn();
            }
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OnState));
            IntendedStates.MisterState = "On";
        }

        public async Task TurnOff()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OffState));
            foreach (var fan in _fan)
            {
                await fan.TurnOff();
            }
            IntendedStates.MisterState = "Off";
        }

        public async Task ActOnLimits()
        {
            var sensorList = AD.ReadingList.Where(i => i.GetType() == typeof(DHT11Sensor));
            double avgHumidity = 0;
            if (sensorList.Count() > 0)
            {
                foreach (var sensor in sensorList)
                {
                    avgHumidity += ((DHT11Sensor)sensor).Humidity;
                }
                avgHumidity = avgHumidity / sensorList.Count();
            }
            await ActOnLimits((int)avgHumidity);
        }

        public async Task ActOnLimits(int CurrentValue)
        {
            if (CurrentValue < LowerLimit)
            {
                await TurnOn();
            }

            else if (CurrentValue > UpperLimit)
            {
                await TurnOff();
            }
        }

        public void RecordPinState(PinState currentState)
        {
            _CurrentState = currentState;
        }

        public async Task Setup()
        {

            await AD.Communicator.WriteAsync(Crypter.SetPinMode(Pin, PinMode.Output));
            await TurnOff();
        }
    }

    public class LightRelayAutomaticControllable : IArduinoAutomaticControllable
    {
        public int Pin { get; }
        public PinState OnState { get; }
        public PinState OffState { get; }
        public int LowerLimit { get; }
        public int UpperLimit { get; }
        public PinState CurrentState { get { return _CurrentState; } }
        private PinState _CurrentState;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pin"></param>
        /// <param name="onState"></param>
        /// <param name="offState"></param>
        /// <param name="lowerLimit">Time to turn on as int in HHMM 24-hr format</param>
        /// <param name="upperLimit">Time to turn off as int in HHMM 24-hr format</param>
        LightRelayAutomaticControllable(int pin, PinState onState, PinState offState, int lowerLimit, int upperLimit)
        {
            Pin = pin;
            OnState = onState;
            OffState = offState;
            LowerLimit = lowerLimit;
            UpperLimit = upperLimit;
        }

        public async Task TurnOn()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OnState));
            IntendedStates.LightState = "On";
        }

        public async Task TurnOff()
        {
            await AD.Communicator.WriteAsync(Crypter.DigitalWrite(Pin, OffState));
            IntendedStates.LightState = "Off";
        }

        public async Task ActOnLimits()
        {
            var currentTime = DateTime.Now;
            int convertedTime = (currentTime.Hour * 100) + currentTime.Minute;
            await ActOnLimits(convertedTime);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CurrentValue">Current time as int with format HHMM</param>
        /// <returns></returns>
        public async Task ActOnLimits(int CurrentValue)
        {
            try
            {
                if (CurrentState == OffState && CurrentValue > LowerLimit && CurrentValue < UpperLimit)
                {
                    await TurnOn();
                }

                else if (CurrentState == OnState && (CurrentValue < LowerLimit || CurrentValue > UpperLimit))
                {
                    await TurnOff();
                }
            }
            catch (Exception)
            {
            }
        }

        public void RecordPinState(PinState currentState)
        {
            _CurrentState = currentState;
        }

        public async Task Setup()
        {

            await AD.Communicator.WriteAsync(Crypter.SetPinMode(Pin, PinMode.Output));
            await TurnOff();
        }
    }

    #endregion


}


