﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWP_Environmental_Master
{
    /// <summary>
    /// Collection of states representing what the program thinks is happening in the physical world
    /// </summary>
    public static class IntendedStates
    {
        public static string FanState = "Off";
        public static string MisterState = "Off";
        public static string HeatState = "Off";
        public static string LightState = "Off";
    }
}
