﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Storage.Streams;


//TODO: Research if VID and PID are consistent between boards
//TODO: figure out if the comment below is actually needed
/*
 * Add the following to app manifest to connect an Arduino Uno:
 * <DeviceCapability Name="serialcommunication">
      <Device Id="vidpid:2341 0043">
        <Function Type="name:serialPort"/>
      </Device>
    </DeviceCapability>
 * */
namespace UWPSerialCommunication
{
    public class SerialCommunicator
    {
        public DataWriter dataWriteObject;
        public DataReader dataReaderObject;
        public SerialDevice port;
        public Queue<string> IncomingStringsQueue = new Queue<string>();
        public bool Listening { get { return _Listening; } }
        private bool _Listening = false;

        public bool StringsAvailable
        {
            get
            {
                return IncomingStringsQueue.Count > 0;
            }
        }

        //TODO: Refer to this at some point
        public string NextString
        {
            get
            {
                if (StringsAvailable)
                {
                    return IncomingStringsQueue.Dequeue();
                }
                else
                {
                    throw new Exception("Results Queue Empty");
                }
            }
        }

        public SerialCommunicator() { }
        
        private TimeSpan eventDelay = new TimeSpan(0, 0, 0, 0, 50);
        private DateTime eventLastTriggered = DateTime.Now;
        public event EventHandler StringReceived;
        protected virtual void OnStringReceived (EventArgs e)
        {
            if (StringReceived != null /*&& (DateTime.Now - eventLastTriggered) > eventDelay*/)
            {
                eventLastTriggered = DateTime.Now;
                StringReceived.Invoke(this, e);
            }
        }

        public async Task ConnectAsync()
        {

            try
            {
                var deviceSelector = SerialDevice.GetDeviceSelectorFromUsbVidPid(0x2341, 0x0043);
                DeviceInformationCollection devices = await DeviceInformation.FindAllAsync(deviceSelector);
                port = await SerialDevice.FromIdAsync(devices[0].Id);
                port.BaudRate = 9600;
                port.Parity = SerialParity.None;
                port.StopBits = SerialStopBitCount.One;
                port.DataBits = 8;

                dataWriteObject = new DataWriter(port.OutputStream);
                _Listening = true;
                Listen();
            }
            catch (Exception)
            {
                _Listening = false;
            }

        }


        private async void Listen()
        {
            try
            {
                if (port != null)
                {
                    dataReaderObject = new DataReader(port.InputStream);

                    // keep reading the serial input
                    while (_Listening)
                    {
                        await ReadAsync();
                    }
                }
            }
            catch (Exception e)
            { }
            finally
            { }
        }

        private async Task ReadAsync()
        {
            uint ReadBufferLength = 1024;
            dataReaderObject.InputStreamOptions = InputStreamOptions.Partial;

            port.ReadTimeout = TimeSpan.FromMilliseconds(5);
            await dataReaderObject.LoadAsync(ReadBufferLength);
            // Launch the task and wait
            if (dataReaderObject.UnconsumedBufferLength > 0)
            {
                IncomingStringsQueue.Enqueue(dataReaderObject.ReadString(dataReaderObject.UnconsumedBufferLength));
                OnStringReceived(new EventArgs());
            }
        }

        public void Disconnect()
        {
            _Listening = false;
            port.Dispose();
        }

        /// <summary>
        /// Write a command to the arduino
        /// </summary>
        /// <param name="text">Encoded command for the arduino</param>
        /// <returns></returns>
        public async Task WriteAsync(string text)
        {
            try
            {
                await Task.Run(() =>
                {
                    if (_Listening)
                    {
                        Task<UInt32> storeAsyncTask;
                        dataWriteObject.WriteString(text);
                        storeAsyncTask = dataWriteObject.StoreAsync().AsTask();
                    }
                });

            }
            catch (Exception e)
            {
                Disconnect();
                throw e;
            }

        }
    }
}


namespace UWPSerialCommunication.Arduino
{
    public enum CommandType
    {
        ReadAll,
        SetPinMode,
        DigitalRead,
        DigitalWrite,
        AnalogRead,
        AnalogWrite,
        ReadDHT11
    }

    public enum PinMode
    {
        Input,
        Output
    }

    public enum PinState
    {
        Low,
        High
    }



    public static class Crypter
    {
        private static string _endCharacter = "x";
        private static string _splitCharacter = ";";
        public static string SetPinMode(int pin, PinMode pinMode)
        {
            return AddEndCharacter(String.Format("{1}{0}{2}{0}{3}", _splitCharacter, (int)CommandType.SetPinMode, pin, (int)pinMode));
        }
        public static string DigitalRead(int pin)
        {
            return AddEndCharacter(String.Format("{1}{0}{2}", _splitCharacter, (int)CommandType.DigitalRead, pin));
        }
        public static string DigitalWrite(int pin, PinState state)
        {
            return AddEndCharacter(string.Format("{1}{0}{2}{0}{3}", _splitCharacter, (int)CommandType.DigitalWrite, pin, (int)state));
        }
        public static string AnalogRead(int pin)
        {
            return AddEndCharacter(string.Format("{1}{0}{2}", _splitCharacter, (int)CommandType.AnalogRead, pin));
        }
        public static string AnalogWrite(int pin, int value)
        {
            if (value < 0)
                value = 0;
            else if (value > 255)
                value = 255;
            return AddEndCharacter(string.Format("{1}{0}{2}{0}{3}", _splitCharacter, (int)CommandType.AnalogWrite, pin, value));
        }
        public static string ReadDHT11(int pin)
        {
            return AddEndCharacter(string.Format("{1}{0}{2}", _splitCharacter, (int)CommandType.ReadDHT11, pin));
        }

        private static string AddEndCharacter(string str)
        {
            return str + _endCharacter;
        }
    }

    public class ArduinoReading
    {
        public CommandType ReadingType;
        public int Pin = -1;
        public double Reading1 = -1.0;
        public double Reading2 = -1.0;

        private List<string> _splitReadingList = new List<string>();

        public ArduinoReading(string incomingString)
        {

            try
            {
                _splitReadingList = incomingString.Split(';').ToList<string>();
                ReadingType = (CommandType)Convert.ToInt16(_splitReadingList[0]);
                int.TryParse(_splitReadingList[1], out Pin);
                double.TryParse(_splitReadingList[2], out Reading1);
                if (_splitReadingList.Count >= 4)
                    double.TryParse(_splitReadingList[3], out Reading2);
            }
            catch (Exception)
            {
            }
        }
    }
}
