﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWP_Environmental_Master
{
    public static class HelpfullFunctions
    {
        public static int Map(int value, int fromLow, int fromHigh, int toLow, int toHigh)
        {
            return (value - fromLow) * (toHigh - toLow) / (fromHigh - fromLow) + toHigh;
        }

        public static int Constrain(int value, int min, int max)
        {
            if (value > max)
            {
                return max;
            }

            else if (value < min)
            {
                return min;
            }

            else return value;
        }

        public static int ProgressToward(int start, int end)
        {
            int difference = end - start;
            int direction = difference - Math.Abs(difference);
            if (direction == 0)
            {
                return 1;
            }
            return -1;
        }
    }
}
